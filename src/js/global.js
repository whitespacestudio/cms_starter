/*
 * Javascript used everywhere.
 * The site_url variable should be defined before referencing this file or expect trouble.
 */
/* global site_url: true */ 
$(function(){

	$("html").removeClass("preload");

	/* !ALERTS */
	/*-----------------------------------------------------------------------------------*/

	if($("#alert_messages > .alert").length)
	{
		$("#alert_messages > .alert").hide(); 
		$("#alert_messages").show();
        $("#alert_messages > .alert").fadeIn(500,function(){
            setTimeout(function(){ // Wait 3s before fading out
	            $('#alert_messages > .alert').fadeOut(2000, function(){
		            $("#alert_messages").hide();
	            });
	        }, 3000);
    	});	
	}
	
});
