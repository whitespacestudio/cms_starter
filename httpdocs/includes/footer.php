<?php
/**
 * The page footer.
 * Included on every page. Includes the footer element and closes the wrapper elements.
 * Also includes the share module.
 */
?>
			<footer>
				
				<div class="container">
					
					<div class="inner">
						
					</div>
												
					<div class="footer_text">
						<p id="copyright">&copy; Copyright <?=date('Y')?>.</p>
						<p id="credit"><a href="http://www.nexusdp.co.uk/" target="_blank">Site by Nexus <img src="<?=SITE_URL?>images/nexus_icon.svg" width="16" height="16"/></a></p>
					</div>
												
				</div>
											
			</footer>
		
		</div> <!-- #blur -->
	</div> <!-- #container -->

	<?php include(SRV_ROOT."includes/modules/share-page.php"); ?>
	
</div> <!-- #wrap -->
