<?php
/**
 * Frontend functions specific to this site
 */
 
/**
 * Constructs a navigation menu.
 * 
 * @param string $menu
 * @param int $parent
 * @param string $build
 * @param int $level
 * @param int $depth
 * @param string $slug
 * @param bool $sef (default: true)
 * @param string $separator (default: '')
 * @param string $class (default: '')
 * @param int $current (default: 0)
 * @param bool $opening_ul (default: true)
 * @param bool $closing_ul (default: true)
 * @return string
 */
function buildMenu($menu,$parent,$build,$level,$depth,$slug,$sef=true,$separator='',$class='',$current=0,$opening_ul = true,$closing_ul = true) {
   	global $thisPath,$tr_page_path,$main_page_pos,$active_path;
   	if(!isset($active_path))
   	{
		$active_path = array();
		$active = new page($current);
		if($active->id !='')
		{
			foreach($active->get_path() as $node)
			{
				$active_path[] = $node['id'];
			}
		}
   	}
   	
   	$class = $class == '' ? $menu : $class;
    $query = "SELECT page_id
    			FROM `cms_pages`
    			WHERE page_parent = '$parent'
    			AND page_menu = '$menu'
				AND page_active = '1'
    			ORDER BY page_pos";
    $result = dbQuery($query);

	$count = dbRows($result);
    if ($count > 0) 
	{
		if($opening_ul)
		{
			$build .= '<ul class="nav ';
			$build .= $level < 1 ? ' '.$class : '';
			$build .= ' level_'.($level+1);
			$build .= "\"";
			$build .= ">\n";
		}
		$level++;
		$num = 1;
		$idlist = array();
        while ($row = dbAssoc($result))
		{
			$page = new page($row['page_id'],'','',SITE_LANG);
			if($level == 1) { $idlist[] = $page->id; }
			$path = '';
			
			$page_type = $page->get_page_type();
			if($page_type == 'external' || $page_type == 'anchor')
			{
				$path = $page->type_val;
			}
			else
			{
				if($page_type == 'alias' && $page->type_val != '') //Alias
				{
					$alias = new page($page->type_val);
					$page_path = $alias->get_path();
				}
				else
				{
					$page_path = $page->get_path();
				}
				
				foreach($page_path as $node)
				{
					$path .= $node['slug'] != 'home' ? $node['slug']."/" : '';
				}
				$path = SITE_URL.$path;
			}
			$build .= "<li id=\"".$menu."_item_".$page->slug."\" class=\"nav".addLeadingZeros($num);
			$build .= $page->id == $current || in_array($page->id, $active_path) ? " active" : "";
			$build .= $page->id == $current ? " current" : "";
			$build .= $page->menu_class !='' ? " ".$page->menu_class : '';
			$build .= $page->child_count() > 0 ? " parent" : "";
			$build .= $num == $count ? " last" : "";
			$build .= $level==1 ? " toplevel" : "";
			$build .= "\"><a href=\"".$path."\"";
			$build .= $page_type == 'external' ? ' target="_blank"' : '';
			$build .= $page->accesskey != "" ? " accesskey=\"".$page->accesskey."\"" : "";
			$build .= ">" . $page->menuTitle . "</a>";
            $build = $level < $depth || $depth == 0 ? buildMenu($menu,$page->id,$build,$level,$depth,$page->slug,$sef,$separator,$class,$current) : $build;
            $build .= $separator !='' && $num != $count ? " $separator " : "";
            $build .= "</li>\n";
            $num++;
        }
        if($closing_ul)
		{
        	$build .= "</ul>\n";
   		}
    }
 
    return $build;
}

/**
 * Get page banner. 
 * Traverse up through page tree if not added to this page
 * 
 * @param object $tr_page (default: '')
 * @return string
 */
function get_banner($tr_page='') {
	
	$banner = false;
	if($tr_page !='')
	{
		foreach($tr_page->get_path() as $step)
		{
			$pathIDs[] = $step['id'];
		}
		$reversePath = array_reverse($pathIDs);
		foreach($reversePath as $id)
		{
			$p = new page($id);
			$h = $p->get_header_image();
			if($h !='')
			{ 
				$ref = $h;
				break;
			}
		}
	
		if(isset($ref))
		{
			$banner = new image($h);
		}
	}
	return $banner;
}

//Get pages from custom field value
function get_pages_from_cf_val($keyword,$val)
{
	$output = array();
	$query = "SELECT val_pageID 
			  FROM cms_page_fields, cms_page_field_values
			  	WHERE val_fieldID = field_id				  	
			  AND field_keyword = '$keyword'
			  AND val_value = '$val'";
	$result = dbQuery($query);
	if(dbRows($result)>0)
	{
		while($row = dbAssoc($result))
		{
			$output[] = $row['val_pageID'];
		}
	}
	return $output;
}

function image_html($img,$args='')
{
	$output = '';
	if(is_string($args))
	{
		parse_str($args, $args);
	}
	$defaults = array(
	    'link_class'	=> "",
	    'img_class'		=> "",
	    'link_title'	=> "",
	    'size'			=> "full"
	);
	$set = array_intersect_key($args + $defaults, $defaults);
	
	if($img->url !='')
	{
		$output .= '<a';
		if($set['link_class'] != '')
		{
			$output .= ' class="'.$set['link_class'].'"';
		}
		$output .= ' href="'.$img->url.'" target="'.$img->target.'"';
		if($set['link_title'] != '')
		{
			$output .= ' title="'.$set['link_title'].'"';
		}
		$output .= '>';
		
	}
	
	$output .= '<img src="';
	$output .= SITE_URL.UP_DIR.$img->get_scaled_path($set['size']);
	$output .= '"';
	if($set['img_class'] != '')
	{
		$output .= ' class="'.$set['img_class'].'"';
	}
	if($img->alt != '')
	{
		$output .= ' alt="'.$img->alt.'"';
	}
	$output .= '/>';
	
	if($img->url !='')
	{
		$output .= '</a>';
	}
	
	return $output;
}
function get_scaled_image_path($img,$size)
{
	switch($size)
	{
		case "full":
			$output = $img->path;
		break;
		case "thumbnail":
			$output = $img->path_tn;
		break;
		default:
			if($size=='')
			{
				$output = $img->path;
			}
			else
			{
				$imgpath = $img->dir_path.$size."/".$img->filename;
				if(file_exists(SRV_ROOT.UP_DIR.$imgpath))
				{
					$output = $imgpath;
				}
				else
				{
					$output = $img->path;
				}
			}
		break;
	}
	return $output;
}
function get_first_image($content) {
  $first_img = '';
  $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $content, $matches);
  $first_img = isset($matches[1][0]) ? $matches[1][0] : '';

  if(empty($first_img)) {
    return false;
  }
  else
  {
  	return $first_img;
  }
}
function remove_first_image($content) {
	$content = preg_replace("/\< *[img][^\>]*[.]*\>/i","",$content,1);
	return $content;
}

/* Get the 'share' URL for the various social networks */
function get_share_link($service,$args='')
{
	global $globalSettings;
	$output = "";
	if(is_string($args))
	{
		parse_str($args, $args);
	}
	$defaults = array(
	    'share_url'    => '',
	    'thumbnail'    => '',
	    'media'        => '',
	    'title'        => '',
	    'text'         => '',
	    'source'       => '',
	    'handle'       => ''
	);
	$set = array_intersect_key($args + $defaults, $defaults);
	
	$share_url 	= urlencode($set['share_url']);
	$thumbnail 	= urlencode($set['thumbnail']);
	$media 		= urlencode($set['media']);
	$title 		= urlencode($set['title']);
	$source 	= urlencode($set['source']);
	$text 		= urlencode(strip_tags($set['text']));
	$handle		= $set['handle'];
	
	switch($service)
	{
		case "twitter":
			
			//680 x 257
			if($handle == '') $handle = $globalSettings['twitter_username'];
			$qs = array();
			$output = "https://twitter.com/intent/tweet?";
			$qs[] = $share_url !='' ? "url=$share_url" : '';
			$qs[] = $text != '' ? "text=$text" : '';
			$qs[] = $handle !='' ? "via=$handle" : '';
			$output .= implode('&', $qs);
		
		break;
		
		case "pinterest":

			//750 x 316
			$qs = array();
			$output = "http://www.pinterest.com/pin/create/button/?";
			$qs[] = $share_url !='' ? "url=$share_url" : '';
			$qs[] = $media !='' ? "media=$media" : '';
			$qs[] = "description=$title";
			$output .= implode('&', $qs);
		
		break;
		
		case "facebook":
		
			//600 x 400 (ish)
			if($handle == '') $handle = $globalSettings['facebook_id'];
			$qs = array();
			$output = "https://www.facebook.com/sharer/sharer.php?";
			$qs[] = $share_url !='' ? "u=$share_url" : '';
			$output .= implode('&', $qs);

		break;
		
		case "linkedin":
		
			//520 x 570
			$qs = array();
			$output = "http://www.linkedin.com/shareArticle?mini=true&";
			$qs[] = $share_url !='' ? "url=$share_url" : '';
			$qs[] = $title !='' ? "title=$title" : ''; //200 chars
			$qs[] = $source != '' ? "source=$source" : ''; //200 chars
			$qs[] = $text != '' ? "summary=$text" : ''; //256 chars
			$output .= implode('&', $qs);

		break;
		
		case "googleplus":
		
			//480 x 476
			$qs = array();
			$output = "https://plus.google.com/share?";
			$qs[] = $share_url !='' ? "url=$share_url" : '';
			$output .= implode('&', $qs);

		break;
	}
	
	return $output;
}
?>