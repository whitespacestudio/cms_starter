<?php
$orig_searchText = $searchText;

if(strtolower(substr($searchText, -1))=='s')
{
	$searchText = rtrim($searchText, "sS");
}

$search_results = array();
if ($searchText != '')
{
	if (!preg_match("/^[a-zA-Z0-9\-'.\s]{1,40}$/", $searchText)) die("Invalid search");
	// Trim query
	$searchText=ltrim($searchText);
	$searchText=rtrim($searchText);
	
	// Search pages
	$query = "SELECT 	page_id,
						page_menu,
						page_title,
						page_slug,
						text_header,
						text_content
				FROM 	cms_pages, cms_page_text
				WHERE 	(page_title LIKE '%$searchText%'
						OR text_header LIKE '%$searchText%'
						OR text_content LIKE '%$searchText%')
				AND		text_pageID = page_id
				AND		page_active = '1'";
	$result = dbQuery($query);
	if (dbRows($result) > 0)
	{
		while ($row = dbAssoc($result))
		{
			$s_page = new page($row['page_id']);
			extract($row,EXTR_PREFIX_ALL,"s");
			
			
			$s_text_header = str_ireplace($find,$replace,$s_text_header);
			$s_text_content = str_ireplace($find,$replace,$s_text_content);
			$s_text_content = substrws($s_text_content, 300);
			$s_text_content .= "... ";
			//$s_text_content = str_replace($searchText, '<span class="highlight">'.$searchText.'</span>',$s_text_content);					
			
			if($s_page->ishome == 1)
			{
				$path = SITE_URL;
			}
			else
			{
				$s_page_path = $s_page->get_path();
				
				$path='';
				foreach($s_page_path as $node)
				{
					$path .= $node['slug']."/";
				}
				$path = SITE_URL.$path;
			}
			$search_results[] = array(	'title'		=> $s_page->title,
										'header'	=> $s_text_header,
										'excerpt'	=> $s_text_content,
										'url'		=> $path
									);
		}
	}
	
	// Results per page
	$results_per_page = 10;
	//!Pagination
	$pagination = new Zebra_Pagination();
	
	// set the method to the SEO friendly way
	$pagination->method('url');
	
	// Set the base url
	$pagination->base_url(SITE_URL."search/".$_GET['s'],false);
	
	// results per page
	$pagination->records_per_page($results_per_page);
	
	// change the default labels
	//$pagination->labels('Newer posts', 'Older posts');

	// Total results
	$pagination->records(count($search_results));
	
	// Display only the posts for the current page
	$search_results = array_slice(
	    $search_results,
	    (($pagination->get_page() - 1) * $results_per_page),
	    $results_per_page
	);
	
	//Set number of page links
	$pagination->selectable_pages(10);
}
?>
			
	</head>
	<body id="page">
		
		<?php include(SRV_ROOT."includes/header.php"); ?>
		
		<aside class="sidebar">

			<?php include(SRV_ROOT."includes/modules/nav-main.php"); ?>
		
		</aside>
		
		<article class="main">
			<?php 
			$crumbs = array(array('path'=>'','name'=>"Search results"));
			include(SRV_ROOT."includes/modules/breadcrumbs.php"); ?>
					
			<h1><?=lang("SEARCH_RESULTS_FOR")?> "<?=$orig_searchText?>"</h1>
	
			<?php
			if(count($search_results)>0)
			{
				foreach($search_results as $res)
				{
					?>
					<div class="result">
						<h2><a href="<?= $res['url'] ?>"><?= $res['title']; ?></a></h2>
						<?= $res['header'] !='' ? "<h3>".$res['header']."</h3>" : ""; ?>
						<p><?= strip_tags($res['excerpt']); ?> <a class="readmore" href="<?= $res['url'] ?>">[read more]</a></p>
					</div>									
					<?php
				}
				
				$pagination->render();
			}
			else
			{
				?>
				<p><?=lang("EMPTY_SEARCH")?></p>
				<?php
			}
			?>
		</article>