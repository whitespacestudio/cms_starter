<?php
/**
 * Page content module
 * 
 * @var $mod Array of module values e.g:
 * 	Array (
	 	[text_id] = Int
		[text_header] => String 
		[text_content] => String
		[text_layout] => String
		[text_style] => String
	)
 */
if(isset($mod)&&!empty($mod))
{
	$classes = array();
	
	$module = array(
		'id'	 => $mod['text_id'],
		'header' => $mod['text_header'],
		'text'   => $mod['text_content'],
		'layout' => $mod['text_layout'],
		'style'  => $mod['text_style']
	);
	$keyword = 'page_module';
	$header = $module['header'];

	$images = array();
	$query = "SELECT * FROM cms_media WHERE med_refID = '{$module['id']}' AND med_keyword = '$keyword' AND med_type = 'image' ORDER by med_pos";
	$result = dbQuery($query);
	if(dbRows($result)>0)
	{
		while($row = dbAssoc($result))
		{
			$img = new image($row['med_id'],'','','image');
			if($img->id !='')
			{
				$images[] = $img;
			}
		}
	}
	
	$styles = explode(" ", $module['style']);
	$testimonial = in_array("testimonial",$styles) ? true : false;

	//Text replacements
	if($module['text'] !='')
	{
		$module['text'] = str_ireplace($find, $replace, $module['text']);
		preg_match_all("/\[\[INCLUDE\{(.*)\}\]\]/", $module['text'], $matches);
		if(!empty($matches[1]))
		{
			foreach ($matches[1] as $key => $filename) {
			    ob_start();
			    include($filename);
			    $include = ob_get_contents();
			    ob_end_clean();
			    $module['text'] = str_replace($matches[0][$key], $include, $module['text']);
			}
		}
		preg_match_all("/\[\[FUNCTION\{(.*)\}\]\]/", $module['text'], $matches);
		if(!empty($matches[1]))
		{
			foreach ($matches[1] as $key => $function) {
				$func_name = strtok($function,'(');
				preg_match("/\((.*)\)/", $function, $args);
				$args = str_ireplace("'", "", $args[1]); //Remove quotes
				$args = explode(",", $args);
				$output = call_user_func_array($func_name, $args);
			    $module['text'] = str_replace($matches[0][$key], $output, $module['text']);
			}
		}
	}

	$classes[] = $module['layout'];
	
	switch($module['layout'])
	{
		default:			
			//Image on the left, text on the right

			$header = '<h2 class="module_title">'.$header.'</h2>';
 
			$txt = "<div class=\"content_box text\">\n";
			$txt .= $module['text'];
			$txt .= "</div>\n";
			
			$classes[] = "page_module";
			$classes = array_merge($classes, explode(" ", $module['style']));
			?>

			<section class="<?=trim(implode(" ", $classes))?>">
				<div class="container">
					<div class="inner">

						<main>
							<?php
							if($tr_page->showTitle == 1 && !$title_shown)
							{
								?>
								<h1><?=$tr_page->title?></h1>
								<?php
								$title_shown = true;
							}
							?>							
							<?=$header?>
							<?=str_ireplace($find, $replace, $txt);?>
						</main>
						<?php
						if(count($images > 0))
						{
							?>
							<aside>
								<?php
								foreach($images as $img)
								{
									?>
									<div class="content_box image img_holder <?=$img->status?>">
										<div style="background-image: url(<?=SITE_URL.UP_DIR.$img->path?>)"></div>
									</div>
									<?php
								}
								?>
							</aside>
							<?php
						}
						?>
					</div>											
				</div>
			</section>
			<?php
		
		break;		
	}
}
unset($columns);