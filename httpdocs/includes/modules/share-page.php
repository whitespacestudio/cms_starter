<?php
/**
 * Displays sharing icons on the side of the screen
 *
 */
$args = array();
$args['share_url']  = isset($seo) && $seo->canonLink !='' ? $seo->canonLink : false;
$args['thumbnail']  = isset($og_img) && $og_img !='' ? $og_img : false;
$args['media']      = isset($og_img) && $og_img !='' ? $og_img : false;
$args['title']      = isset($pageTitle) && $pageTitle !='' ? $pageTitle : false;
$args['text']       = isset($seo) && $seo->metaDesc !='' ? $seo->metaDesc : false;
if($args['share_url'])
{
	?>
	<div id="share_page">
		<ul class="socnet">
			<li><a href="<?=get_share_link('facebook',$args)?>" target="_blank" class="circle facebook icon" title="Facebook"></a></li>
			<li><a href="<?=get_share_link('twitter',$args)?>" target="_blank" class="circle twitter icon" title="Twitter"></a></li>
			<li><a href="<?=get_share_link('googleplus',$args)?>" target="_blank" class="circle googleplus icon" title="Google+"></a></li>
			<li><a href="<?=get_share_link('pinterest',$args)?>" target="_blank" class="circle pinterest icon" title="Pinterest"></a></li>
			<li><a href="<?=get_share_link('linkedin',$args)?>" target="_blank" class="circle linkedin icon" title="LinkedIn"></a></li>
		</ul>
	</div>
	<?php
}
unset($args);
?>
