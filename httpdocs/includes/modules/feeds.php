<?php
/**
 * Blog and social network feeds
 *
 * Displays the latest posts from the blog, as well as Pinterest, Instagram and Twitter
 * 
 * bool $divider Show or hide the top divider (Default: true)
 * bool $show_blog Show blog posts or... not (Default: true)
 */
?>

<?php
$divider = isset($divider) ? $divider : true;
$show_blog = isset($show_blog) ? $show_blog : true;

$feeds = get_feed_list('keyword=global');

$facebook = array();
$twitter = array();
$instagram = array();
$pinterest = array();
$image_feed = array();

foreach($feeds as $feed_id)
{
	$feed = new feed($feed_id);
	$feed->options = array(
		'ignore_replies'        => true,
		'ignore_retweets'       => false
	);
	if($feed->id !='')
	{
		$acc = new feed_account($feed->accountID);
		$items = $feed->fetch('cached_only=1');
		if(!empty($items))
		{
			foreach($items as $item)
			{
				$item['provider'] = $acc->provider;
				$item['timestamp'] = strtotime($item['time']);
				switch($acc->provider)
				{
					case 'facebook':
						$facebook[] = $item;
						$text_feed[] = $item;
					break;
					case 'twitter':
						$twitter[] = $item;
						if($item['img'] !='')
						{
							$image_feed[] = $item;
						}
						else
						{
							$text_feed[] = $item;
						}
					break;
					case 'instagram':
						$instagram[] = $item;
						$image_feed[] = $item;
					break;
					case 'pinterest':
						$pinterest[] = $item;
						$image_feed[] = $item;
					break;
				}
			}
		}
	}
}
usort($image_feed, function($a, $b) {
	return $a['timestamp'] - $b['timestamp'];
});
usort($text_feed, function($a, $b) {
	return $a['timestamp'] - $b['timestamp'];
});
$image_feed = array_reverse($image_feed);
$text_feed = array_reverse($text_feed);	
?>
<div class="feeds<?= !$divider ? ' nodivider' : ''?>">
	<?php
	if($divider)
	{
		?>
		<div class="circle_divider"></div>
		<?php
	}
	?>
	<div class="container">
		<div class="inner">
			
			<h2 class="alt">News and social</h2>

			<?php
			if(!empty($image_feed))
			{
				$max_images = $show_blog ? 4 : 7;
				?>
				<div class="image_posts">
					<?php
					$c = 0;
					$i = 1;
					foreach($image_feed as $post)
					{
						$title = '';
						$text = $post['txt'];
						$img = $post['img'];
						$provider = $post['provider'];
						$link = $post['lnk'];
						include(SRV_ROOT."includes/modules/feed-image_post.php");
						if($i==4 && $c <= $max_images)
						{
							echo "</div>\n<div class=\"image_posts second_row\">\n";
							$i=1;
						}
						else
						{
							$i++;
						}
						$c++;
						if($c == $max_images)
						{
							break;
						}
					}
					?>
				</div>
				<?php
			}
			?>
			
			<?php
			if($show_blog)
			{
				$blog = new blog(1,'',SITE_LANG);
				
				//!Get path to blog
				$blog_id = $blog->root_page;
				$blog_node = new sitemap_node('',$blog_id,'page');
				$blog_path = $blog_node->path;
				?>
				<div class="blog_posts">
					<?php
					$posts = $blog->get_posts('limit=3');
					foreach($posts as $post_id)
					{
						$post = new blog_post($post_id,'',SITE_LANG);
						//!Get post image
						$post_image = get_post_image($post);
						$post_node = new sitemap_node('',$post->id,'blog_post');
						$post_permalink = SITE_URL.$post_node->path;
						?>
					
						<div class="blog_post">
							
							<a href="<?=$post_permalink?>" class="post_img">
								<div class="inner" style="background-image: url(<?=$post_image['tn']?>)"></div>
							</a>	
		
							<div class="post_text">
								<h3><?=$post->title?></h3>
								<?=$post->get_summary('readmore=0&char_limit=140')?>
							</div>
							
							<a href="<?=$post_permalink?>" class="readmore button">More</a>
						
						</div>
						
						<?php
					}
					?>
				</div>
				<?php
			}
			?>
			
			<div class="twitter_feed">
			
				<?php
				$twitter = new feed('','provider=twitter&keyword=global');
				$tweets = $twitter->fetch('cached_only=1');
				if(count($tweets)>0)
				{
					//Only show 2 tweets when blog posts are omitted
					//So it all lines up, like
					if(!$show_blog && count($tweets)==3)
					{
						array_pop($tweets);
					}
					foreach($tweets as $tweet)
					{
						?>
						<div class="social_post twitter">
				
							<div class="post_text">
								<div class="icon_img">
									<a href="<?=$tweet['lnk']?>">
										<i class="icon bookmark twitter"></i>
									</a>
								</div>
								<p><?=$tweet['txt']?></p>
								<p class="actions"><a href="https://twitter.com/intent/tweet?in_reply_to=<?=$tweet['id']?>" target="_blank"><i class="reply icon"></i> Reply</a> <a href="https://twitter.com/intent/retweet?tweet_id=<?=$tweet['id']?>" target="_blank"><i class="retweet icon"></i> Retweet</a></p>
							</div>
							
						</div>
						<?php
					}
				}
				?>
			
			</div>

		</div>
					
	</div>
	
</div>
<?php
unset($divider,$show_blog);
?>
