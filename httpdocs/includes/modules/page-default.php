<?php
/** 
 * The standard page title and text layout
 *
 * Requires a page object named `$tr_page`
 */
 
switch($pageSettings['page_content_entry'])
{
	case 'SIMPLE':

		//!Simple editing method
		$textblocks = array(
			'primary' => $tr_page->text('keyword=primary'),
			'secondary' => $tr_page->text('keyword=secondary'),
			'tertiary' => $tr_page->text('keyword=tertiary')
		);
		?>
		<div class="container">
			<div class="inner">
				<main>
					
					<?php
					if($tr_page->showTitle == 1)
					{
						?>
						<h1><?=$tr_page->title?></h1>
						<?php
					}
					
					if($textblocks['primary'] !='')
					{
						preg_match_all("/\[\[INCLUDE\{(.*)\}\]\]/", $textblocks['primary'], $matches);
						if(!empty($matches[1]))
						{
							foreach ($matches[1] as $key => $filename) {
							    ob_start();
							    include($filename);
							    $include = ob_get_contents();
							    ob_end_clean();
							    $textblocks['primary'] = str_replace($matches[0][$key], $textblocks['primary'], $include);
							}
						}
						preg_match_all("/\[\[FUNCTION\{(.*)\}\]\]/", $textblocks['primary'], $matches);
						if(!empty($matches[1]))
						{
							foreach ($matches[1] as $key => $function) {
								$func_name = strtok($function,'(');
								preg_match("/\((.*)\)/", $function, $args);
								$args = str_ireplace("'", "", $args[1]); //Remove quotes
								$args = explode(",", $args);
								$output = call_user_func_array($func_name, $args);
							    $textblocks['primary'] = str_replace($matches[0][$key], $textblocks['primary'], $output);
							}
						}
						echo $textblocks['primary'];
					}
					?>
					
				</main>
				<?php
				if(count($tr_page->images > 0))
				{
					?>
					<aside>
						<?php
						foreach($tr_page->images as $img)
						{
							$img = new image($img['med_id']);
							?>
							<div class="img_holder">
								<div style="background-image: url(<?=SITE_URL.UP_DIR.$img->path_tn?>)"></div>
							</div>
							<?php
						}
						?>
					</aside>
					<?php
				}
				?>
			</div>					
		</div>
		<?php		
		
	break;
	
	case 'MODULAR':
	
		$title_shown = false;
		//!Modular editing method
		$modules = $tr_page->get_modules();
		if(!empty($modules))
		{
			foreach($modules as $mod)
			{
				include(SRV_ROOT."includes/modules/page_content_module.php");
			}
		}
		else
		{
			if($tr_page->showTitle == 1 && !$title_shown)
			{
				?>
				<h1><?=$tr_page->title?></h1>
				<?php
				$title_shown = true;
			}
		}
		
	break;
}
?>						
