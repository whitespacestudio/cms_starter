<?php
/**
 * Single post preview box for social network feeds
 * 
 * @var $title String
 * @var $title_link String
 * @var $text String
 * @var $provider String e.g 'facebook'
 * @var $thumb_img
 * @var $icon_img String
 * @var $icon_link String
 */
?>
<div class="social_post<?=isset($provider)?' '.strtolower($provider):'';?>">
	
	<?php
	switch($provider)
	{
		default:
			?>
			<?php
			if(isset($thumb_img) && $thumb_img !='')
			{
				?>
				<div class="post_img">
					<img src="<?=$thumb_img?>" alt=""/>
				</div>
				<?php
			}
			?>
			<div class="post_text">
				<?php
				if(isset($icon_img) && $icon_img !='')
				{
					?>
					<div class="icon_img">
						<?php
						if(isset($icon_link) && $icon_link !='')
						{	
							?>
							<a href="<?=$icon_link?>" target="_blank">
							<?php
						}
						?>
						<img src="<?=$icon_img?>" alt="" />
						<?php
						if(isset($icon_link) && $icon_link !='')
						{	
							?>
							</a>
							<?php
						}
						?>
					</div>
					<?php
				}
				else
				{
					if(isset($provider) && $provider !='')
					{
						?>
						<div class="icon_img">
							<?php
							if(isset($icon_link) && $icon_link !='')
							{	
								?>
								<a href="<?=$icon_link?>">
								<?php
							}
							?>
							<i class="icon bookmark <?=strtolower($provider)?>"></i>
							<?php
							if(isset($icon_link) && $icon_link !='')
							{	
								?>
								</a>
								<?php
							}
							?>
						</div>
						<?php
					}
				}
				?>
				<?php
				if(isset($title) && $title !='')
				{
					if(isset($title_link) && $title_link !='')
					{	
						?>
						<a href="<?=$title_link?>">
						<?php
					}
					?>
					<h3><?=$title?></h3>
					<?php
					if(isset($title_link) && $title_link !='')
					{	
						?>
						</a>
						<?php
					}
				}
				?>
				<?php
				if(isset($text) && $text !='')
				{
					?>
					<p><?=$text?></p>
					<?php
				}
				?>
			</div>
			<?php
		break;
	}
	?>
</div>

<?php
unset($title,$title_link,$text,$provider,$thumb_img,$icon_img,$icon_link);
?>