<?php
/**
 * Displays a single image post from a social network feed.
 *
 * $title
 * $text
 * $img
 * $provider
 * $link
 */
?>

<div class="social_post <?=$provider?>">

	<a href="<?=$link?>" target="_blank" class="post_img">
		<div class="inner" style="background-image: url(<?=$img?>)"></div>
	</a>	
	
	<div class="post_icon">
		<img src="<?=SITE_URL?>images/<?=$provider?>.png" alt="<?=ucwords($provider)?>"/>
	</div>
					
</div>
