<?php
//!Catch form submissions
if(empty($fields))
{
	$contactemails = $globalSettings['admin_email'];
	$fields = formSubmit($contactemails);
}
//!Get the current page ID and URL name
$page_slug = isset($tr_page->slug) ? $tr_page->slug : (isset($this_slug) ? $this_slug : '');
$page_id = isset($tr_page->id) ? $tr_page->id : '';
$activeID = $page_id;

//!Set the SEO object
$seo = isset($seo) ? $seo : new seo;
$seo_refID = isset($seo_refID) ? $seo_refID : $page_id;
$seo_slug = isset($seo_slug) ? $seo_slug : $page_slug;
$seo_keyword = isset($seo_keyword) ? $seo_keyword : '';
if($seo_refID != '')
{
	$seo = new seo($seo_refID,$seo_keyword,SITE_LANG);
}
$seo->canonLink = isset($permalink) ? $permalink : "";	

//!Set the page title
$pageTitle = isset($seo) && $seo->pageTitle != '' ? $seo->pageTitle : $tr_page->title." - ".SITE_NAME;

//!Content specific variables
$twitter_card = "summary_large_image";
switch($view)
{
	case "page":
		//poster image
		$himg = new image($tr_page->get_header_image());
		if($himg->id !='')
		{
			$og_img = SITE_URL.UP_DIR.$himg->path;
		}
		else
		{
			$pimg = new image($tr_page->get_menu_image());
			if($pimg->id !='')
			{
				$og_img = SITE_URL.UP_DIR.$pimg->path;
			}
		}
	break;

	case "home":
	default:
		//poster image
		$himg = new image($tr_page->get_header_image());
		if($himg->id !='')
		{
			$og_img = SITE_URL.UP_DIR.$himg->path;
		}
		else
		{
			$pimg = new image($tr_page->get_menu_image());
			if($pimg->id !='')
			{
				$og_img = SITE_URL.UP_DIR.$pimg->path;
			}
		}
	break;
}
?>
<!DOCTYPE html>
<html lang="en" class="no-js preload">
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1">

<!-- !Favicon and touch icons -->
<link rel="shortcut icon" href="<?=SITE_URL?>images/favicon.ico">
<link rel="apple-touch-icon" href="<?=SITE_URL?>images/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?=SITE_URL?>images/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?=SITE_URL?>images/apple-touch-icon-114x114.png">

<title><?php echo $pageTitle; ?></title>
<script src="<?=SITE_URL?>js/modernizr.js"></script>

<?php
//!Output SEO meta 
$seo->render_meta();
//!Output language links
$tr_page->render_language_alt_urls()
?>

<!-- For Facebook / Google+ -->
<meta property="og:title" content="<?= $pageTitle; ?>" />
<meta property="og:type" content="article" />
<?php
if(isset($og_img) && $og_img !='')
{
	?>
	<meta property="og:image" content="<?=$og_img?>" />
	<?php
}
?>
<meta property="og:url" content="<?=$seo->canonLink?>" />
<?php
if(isset($seo->metaDesc) && $seo->metaDesc != '')
{
	$og_desc = htmlentities($seo->metaDesc);
}
if(isset($og_desc))
{
	?>
	<meta property="og:description" content="<?=htmlentities($og_desc)?>" />
	<?php
}
?>
<!-- For Twitter -->
<meta name="twitter:card" content="<?=$twitter_card?>">
<meta name="twitter:url" content="<?=$seo->canonLink?>" />
<?php
if($globalSettings['twitter_username']!='')
{
	?>
	<meta name="twitter:site" content="@<?=$globalSettings['twitter_username']?>">
	<meta name="twitter:creator" content="@<?=$globalSettings['twitter_username']?>">
	<?php
}
?>
<meta name="twitter:title" content="<?= $pageTitle; ?>">
<?php
if(isset($og_desc))
{
	?>
	<meta name="twitter:description" content="<?=htmlentities($og_desc)?>">
	<?php
}
if(isset($img_gallery) && !empty($img_gallery))
{
	$i=0;
	foreach($img_gallery as $gallery_img)
	{
		if($i<4)
		{
			?>
			<meta name="twitter:image<?=$i?>" content="<?=$gallery_img?>">
			<?php
			$i++;
		}
	}
}
else
{
	if(isset($og_img) && $og_img !='')
	{
		?>
		<meta name="twitter:image" content="<?=$og_img?>">
		<?php
	}	
}
?>

<!--!CSS-->
<link rel="stylesheet" type="text/css" media="all" href="<?=SITE_URL?>css/styles.css?v=20170101"/>

<!--!JS-->
<?= $globalSettings['tracker_code']; ?>

<script>
	var site_url = "<?=SITE_URL?>";
</script>

<script src="<?=SITE_URL?>js/jquery-3.0.0.min.js"></script>
<script src="<?=SITE_URL?>js/ScrollMagic.min.js"></script>
<script src="<?=SITE_URL?>js/iscroll-min.js"></script>
<script src="<?=SITE_URL?>js/jquery.fancybox.pack.js"></script>
<script src="<?=SITE_URL?>js/jquery.validate.min.js"></script>
<script src="<?=SITE_URL?>js/CSSPlugin.min.js"></script>
<script src="<?=SITE_URL?>js/EasePack.min.js"></script>
<script src="<?=SITE_URL?>js/TweenLite.min.js"></script>
<script src="<?=SITE_URL?>js/jquery.cycle2.min.js"></script>
<script src="<?=SITE_URL?>js/global-min.js?v=20170101"></script>
