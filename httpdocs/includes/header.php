<?php
/**
 * The page header.
 * Included on every page. Includes the header element and opens any wrapper elements.
 */
?>
<div id="wrap">

	<div id="alert_messages" style="display: none">
		<?php displayMessage(); ?>
	</div>

	<div id="container">
		<?php include(SRV_ROOT."includes/modules/nav-mobile.php"); ?>

		<header>
			
			<div class="container">
				
				<a id="site_logo" href="<?=SITE_URL?>">
					<picture>
<!--
						<source srcset="<?=SITE_URL?>images/logo_mobile.svg" media="(max-width: 600px)">
						<img src="<?=SITE_URL?>images/logo.svg" alt="<?=SITE_NAME?>">
-->
					</picture>
				</a>
								
			</div>

			<nav>
				<div class="container">
					<ul class="mainNav">
						<?php
						echo buildMenu('mainNav',0,'',0,1,'',true,'','',$tr_page->id,false,false);
						?>
					</ul>
				</div>									
			</nav>			
			
			<button class="hamburger">
				<span class="hamburger-box">
					<span class="hamburger-inner"></span>
				</span>
			</button>
			
		</header>
		
		<div id="blur">
