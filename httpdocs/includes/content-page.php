<?php
/**
 * Standard page template.
 * Content is included based on page type, using files save in the 'includes' directory named page-[page type].php
 * 
 */

//!PAGE LAYOUTS
$page_type = $tr_page->get_page_type(); //Get the page type
$tr_page_path = $tr_page->get_path(); //Get the path
$tl_page = new page($tr_page_path[0]['id']); //Create a new object for the top level page

$include_file = SRV_ROOT."includes/page-".$page_type.".php"; //Look for files named page-[page type].php
if(file_exists($include_file))
{
	include($include_file);
}
else
{			
	//!DEFAULT PAGE LAYOUT
	?>
	</head>
	<body id="page">
		
		<?php include(SRV_ROOT."includes/header.php"); ?>
		
		<article>
			<?php include(SRV_ROOT."includes/modules/page-default.php"); ?>
		</article>			
		<?php
}
?>