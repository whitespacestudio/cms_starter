<?php
include_once(SRV_ROOT."includes/global-includes.php");
$view='';
$level1 = "404";
$this_slug = $level1;
$parent_slug = '';

$page_details = get_page_details();
if($page_details==false)
{
	die("Page not found");
}

$contactemails = $globalSettings['admin_email'];
$fields = formSubmit($contactemails);
include(SRV_ROOT."includes/meta.php");
?>
</head>
<?php
include(SRV_ROOT."includes/content-page.php");
include(SRV_ROOT."includes/footer.php");
?>
</body>
</html>