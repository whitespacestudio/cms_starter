<?php
$ssl = true;
require_once('../admin/vertigo.php');
require_once('functions.php');
$pageTitle = "Please log in";

login();

if(isset($_GET['logout']))
{
	//Log the user out
	if($user->loggedin) $user->log_out();
	flash_message("success","You have been successfully logged out");
	$pageTitle = "Logged out";
}

//Prevent the user visiting the logged in page if he/she is already logged in
if($user->loggedin) { header("Location: ".SITE_URL."admin/users/routing.php"); exit(); }

include(SRV_ROOT."admin/includes/meta.php");
?>
</head>
<body class="login">
<div id="wrap" class="row">
<?php include(SRV_ROOT."admin/includes/header.php"); ?>
<div class="container">
	<div class="content">
		<?php
		//User feedback
		displayMessage();
		?>
		<form class="loginForm fourcol last centred" method="post" action="index.php">
			<table>
				<caption>Please Log in</caption>
				
				<tr>
					<th><label for="login">Username</label></th>
					<td><input id="username" type="text" name="username"  /></td>
				</tr>
				<tr>
					<th><label for="password">Password</label></th>
					<td><input id="password" type="password" name="password" /></td>
				</tr>
				<tr>
					<td colspan="2">
					<label><input type="checkbox" class="checkbox" name="remember_me" value="1" /> Keep me logged in for <?php echo getCookieLength($remember_me_length);?></label>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<span class="left">
							<a href="forgot-password.php">Forgotten your password?</a><br>
							<a href="forgot-username.php">Forgotten your username?</a>
						</span>
						<input name="submit" type="submit" class="button submit right" value="Log in"/>
					</td>
				</tr>
			</table>
		</form>
	</div>
</div>
</div>
<?php include(SRV_ROOT."admin/includes/footer.php"); ?>