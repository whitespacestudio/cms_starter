<?php
$ssl = true;
require_once('../admin/vertigo.php');
require_once('functions.php');

$pageTitle = "Password reset";

//Prevent the user visiting this page if he/she is already logged in
if($user->loggedin == 1) { header("Location: ".SITE_URL."admin/users/routing.php"); exit(); }

$fields = forgotPassword();

include(SRV_ROOT."admin/includes/meta.php");
?>
</head>
<body class="login">
<div id="wrap" class="row">
<?php include(SRV_ROOT."admin/includes/header.php"); ?>
<div class="container">
	<div class="content">
		<?php
		//User feedback
		displayMessage();
		?>
		<form class="loginForm fourcol last centred" method="post" action="">
			<table>
				<caption>Password reset</caption>
				<tr>
					<td colspan="2">Please enter your user name and email adress below.<br/>Your password will be reset and emailed to you</td>
				</tr>
				<tr>
					<th><label for="username">Username</label></th>
					<td><input id="username" type="text" name="username" value="<?=isset($fields['username'])?$fields['username']:''?>"/></td>
				</tr>
				<tr>
					<th><label for="email">Email</label></th>
					<td><input id="email" type="text" name="email" value="<?=isset($fields['email'])?$fields['email']:''?>"/></td>
				</tr>
				<tr>
					<td colspan="2">
						<span class="left"><a href="index.php">Back to login</a></span>
						<input name="submit" type="submit" class="button submit right" value="Submit"/>
					</td>
				</tr>
			</table>
		</form>
	</div>
</div>
</div>
<?php include(SRV_ROOT."admin/includes/footer.php"); ?>