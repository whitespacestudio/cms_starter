<?php
function login()
{
	global $db,$db_table_prefix,$remember_me_length,$user;
	if(!empty($_POST))
	{
		$errors = array();
		$username = trim($_POST["username"]);
		$password = trim($_POST["password"]);
		$remember_choice = isset($_POST["remember_me"]) ? trim($_POST["remember_me"]) : 0;
		
		//Form validation
		if($username == "")
		{
			$errors[] = lang("ACCOUNT_SPECIFY_USERNAME");
		}
		if($password == "")
		{
			$errors[] = lang("ACCOUNT_SPECIFY_PASSWORD");
		}
			
		//End data validation
		if(count($errors) == 0)
		{
			//Username does not exist
			if(!usernameExists($username))
			{
				$errors[] = lang("ACCOUNT_USER_OR_PASS_INVALID");
			}
			else
			{
				$user->set_by_username($username);
			
				//Is the user's account active?
				if($user->user_active == 0)
				{
					$errors[] = lang("ACCOUNT_INACTIVE");
					
				}
				else
				{
					if($user->validate_password($password) == false)
					{
						//Password incorrect.
						$errors[] = lang("ACCOUNT_USER_OR_PASS_INVALID");
					}
					else
					{
						//Passwords match
						//Set some new properties
						$user->remember_me = $remember_choice;
						$user->remember_me_sessid = generateHash(uniqid(rand(), true));
						$user->loggedin = true;
						
						
						//Update last sign in
						$user->updateLastSignIn();
		
						if($user->remember_me == 0)
						{
							$_SESSION[SESSNAME] = $user;
						}
						else if($user->remember_me == 1) 
						{
							dbQuery("INSERT INTO `cms_remember_me` VALUES('".time()."', '".serialize($user)."', '".$user->remember_me_sessid."')");
							setcookie(SESSNAME, $user->remember_me_sessid, time()+parseLength($remember_me_length));
						}
						
						//Redirect to user routing page
						header("Location: ".SITE_URL."admin/users/routing.php");
						exit;
					}
				}
			}
		}
		if(count($errors) > 0)
        {
        	//Errors in login. Clear user object and display error messages.
        	$user = new user;
        	errorBlock($errors);
        }
	}
}

function forgotPassword()
{
	global $db,$db_table_prefix,$remember_me_length,$globalSettings;
	
	$errors = array(); // set the errors array to empty, by default
	$fields = array(); // stores the field values
	$rules = array(); // stores the validation rules
		
	//User has confirmed they want their password changed
	//----------------------------------------------------------------------------------------------
	if(!empty($_GET["confirm"]))
	{
		$token = trim($_GET["confirm"]);
		
		if($token == "" || !validateActivationToken($token,TRUE))
		{
			$errors[] = lang("FORGOTPASS_INVALID_TOKEN");
		}
		else
		{
			$rand_pass = getUniqueCode(15);
			$secure_pass = generateHash($rand_pass);
			
			$lostuser = new user;
			$lostuser->set_by_token($token);

			if(count($errors) == 0)
			{								
				//Setup our custom hooks
				$find = array("#GENERATED-PASS#","#FIRSTNAME#");
				$replace = array($rand_pass,$lostuser->user_firstName);

				$html_content = file_get_contents(SRV_ROOT."templates/mail/your-lost-password.php");
				$html_content = str_replace($find,$replace,$html_content);
				
				$h2t = new \Html2Text\Html2Text($html_content);
				$plain_text = $h2t->get_text();
				
				$html = file_get_contents(SRV_ROOT.'templates/mail/email_header.php') .
						$html_content .
						file_get_contents(SRV_ROOT.'templates/mail/email_footer.php');
									
				$mail = new PHPMailer(true); 
				
				try 
				{
					$recipient = $lostuser->user_email;
					$sender_email = $globalSettings['admin_email'];
					$sender_name = $globalSettings['from_name'];
					$subject = "Your new password for ".SITE_NAME;

					$mail->AddAddress($recipient);
					$mail->SetFrom($sender_email, $sender_name);
					$mail->AddReplyTo($sender_email, $sender_name);
					$mail->Subject = $subject;
					$mail->AddEmbeddedImage(SRV_ROOT.'images/'.ELOGO, SESSNAME.'_logoimg', ELOGO);
					$mail->MsgHTML($html);
					$mail->AltBody = $plain_text;
					if ($mail->Send())
					{
						if(!updatePasswordFromToken($secure_pass,$token))
						{
							$errors[] = lang("SQL_ERROR");
						}
						else
						{
							flagLostPasswordRequest($lostuser->user_nameClean,0);
							$success_message = lang("FORGOTPASS_NEW_PASS_EMAIL");
							$redirect = SITE_URL."admin/users/edit.php?id=".$lostuser->user_id;
						}
					}
				} 
				catch (phpmailerException $e) 
				{
					$errors[] = $e->errorMessage();
				}
				
			}				
		}
	}
	
	//----------------------------------------------------------------------------------------------
	
	//User has denied this request
	//----------------------------------------------------------------------------------------------
	if(!empty($_GET["deny"]))
	{
		$token = trim($_GET["deny"]);
		
		if($token == "" || !validateActivationToken($token,TRUE))
		{
			$errors[] = lang("FORGOTPASS_INVALID_TOKEN");
		}
		else
		{
			$lostuser = new user;
			$lostuser->set_by_token($token);

			flagLostPasswordRequest($lostuser->user_nameClean,0);
			$success_message = lang("FORGOTPASS_REQUEST_CANNED");
		}
	}
	
	
	
	
	//----------------------------------------------------------------------------------------------
	
	
	//Forms posted
	//----------------------------------------------------------------------------------------------
	if(!empty($_POST))
	{	
		$email = escapeInput($_POST["email"]);
		$username = escapeInput($_POST["username"]);

		$rules[] = "required,username,".lang("ACCOUNT_SPECIFY_USERNAME");
		$rules[] = "required,email,".lang("ACCOUNT_SPECIFY_EMAIL");
		$rules[] = "valid_email,email,".lang("ACCOUNT_INVALID_EMAIL");

		//Perform some validation
		$errors = validateFields($_POST, $rules);		
		
		if(count($errors) == 0)
		{		
			//Check that the username / email are associated to the same account
			if(!emailUsernameLinked($email,$username))
			{
				$errors[] =  lang("ACCOUNT_USER_OR_EMAIL_INVALID");
			}
			else
			{
				//Check if the user has any outstanding lost password requests
				$lostuser = new user;
				$lostuser->set_by_username($username);
				
				if($lostuser->user_lostPasswordRequest == 1)
				{
					$errors[] = lang("FORGOTPASS_REQUEST_EXISTS");
				}
				if(count($errors) == 0)
				{
					//Email the user asking to confirm this change password request
					//We use the activation token again for the url key it gets regenerated everytime it's used.
					
					$confirm_url = lang("CONFIRM")."\n".SITE_URL."login/forgot-password.php?confirm=".$lostuser->user_activationToken;
					$deny_url = ("DENY")."\n".SITE_URL."login/forgot-password.php?deny=".$lostuser->user_activationToken;
					
					//Setup our custom hooks
					$find = array("#DATE#","#CONFIRM-URL#","#DENY-URL#","#FIRSTNAME#");
					$replace = array(date("D dS F Y, g:i a"),$confirm_url,$deny_url,$lostuser->user_firstName);

					
					$html_content = file_get_contents(SRV_ROOT."templates/mail/lost-password-request.php");
					$html_content = str_replace($find,$replace,$html_content);
					
					$h2t = new \Html2Text\Html2Text($html_content);
					$plain_text = $h2t->get_text();
					
					$html = file_get_contents(SRV_ROOT.'templates/mail/email_header.php') .
							$html_content .
							file_get_contents(SRV_ROOT.'templates/mail/email_footer.php');
										
					$mail = new PHPMailer(true); 
					//defaults to using php "mail()"; the true param means it will throw exceptions on errors, which we need to catch
					
					try 
					{
						$recipient = $email;
						$sender_email = $globalSettings['admin_email'];
						$sender_name = $globalSettings['from_name'];
						$subject = "Lost password request from ".SITE_NAME;

						$mail->AddAddress($recipient);
						$mail->SetFrom($sender_email, $sender_name);
						$mail->AddReplyTo($sender_email, $sender_name);
						$mail->Subject = $subject;
						$mail->AddEmbeddedImage(SRV_ROOT.'images/'.ELOGO, SESSNAME.'_logoimg', ELOGO);
						$mail->MsgHTML($html);
						$mail->AltBody = $plain_text;
						if ($mail->Send())
						{
							//Update the DB to show this account has an outstanding request
							flagLostPasswordRequest($username,1);
							$success_message = lang("FORGOTPASS_REQUEST_SUCCESS");
							$redirect = "index.php";
						}
					} 
					catch (phpmailerException $e) 
					{
						$errors[] = $e->errorMessage();
					}
					
				}
			}
		}
	}
	if(count($errors) > 0)
	{
		$fields = stripslashes_array($_POST); // re-populate the form fields
		errorBlock($errors);
	}
	if(isset($success_message)) flash_message("success", $success_message);
	if(isset($redirect)) { header("location: ".$redirect); exit; }
	if(isset($fields)) return $fields;	
}

function forgotUsername()
{
	global $db,$db_table_prefix,$remember_me_length,$globalSettings;
	
	$errors = array(); // set the errors array to empty, by default
	$fields = array(); // stores the field values
	$rules = array(); // stores the validation rules
		
	if(!empty($_POST))
	{	
		$email = $_POST["email"];
		$firstname = $_POST["firstname"];

		$rules[] = "required,firstname,".lang("ACCOUNT_SPECIFY_FIRSTNAME");
		$rules[] = "required,email,".lang("ACCOUNT_SPECIFY_EMAIL");
		$rules[] = "valid_email,email,".lang("ACCOUNT_INVALID_EMAIL");

		//Perform some validation
		$errors = validateFields($_POST, $rules);		
		
		if(count($errors) == 0)
		{		
			//Look up account based on email and first name
			$lostuser = new user;
			$lostuser->set_by_email_firstname($email,$firstname);
			
			if(empty($lostuser->user_id))
			{
				$errors[] = lang("ACCOUNT_NONEXISTENT_USER");
			}
			else
			{
				//Email the user with their username								
				$find = array("#SITE_NAME#","#DATE#","#FIRSTNAME#","#USERNAME#","#EMAIL#","#PASSWORD_RESET_URL#","#LOGIN_URL#");
				$replace = array(SITE_NAME, date("D dS F Y \a\t g:i a"),$lostuser->user_firstName,$lostuser->user_name,$email,SITE_URL."login/forgot-password.php",SITE_URL."login/");
				
				$html_content = file_get_contents(SRV_ROOT."templates/mail/lost-username-request.php");
				$html_content = str_replace($find,$replace,$html_content);
				
				$h2t = new \Html2Text\Html2Text($html_content);
				$plain_text = $h2t->get_text();
				
				$html = file_get_contents(SRV_ROOT.'templates/mail/email_header.php') .
						$html_content .
						file_get_contents(SRV_ROOT.'templates/mail/email_footer.php');
									
				$mail = new PHPMailer(true); 				
				try 
				{
					$recipient = $email;
					$sender_email = $globalSettings['admin_email'];
					$sender_name = $globalSettings['from_name'];
					$subject = "Username reminder from ".SITE_NAME;

					$mail->AddAddress($recipient);
					$mail->SetFrom($sender_email, $sender_name);
					$mail->AddReplyTo($sender_email, $sender_name);
					$mail->Subject = $subject;
					$mail->AddEmbeddedImage(SRV_ROOT.'images/'.ELOGO, SESSNAME.'_logoimg', ELOGO);
					$mail->MsgHTML($html);
					$mail->AltBody = $plain_text;
					if($mail->Send())
					{
						$success_message = lang("FORGOTUSERNAME_REQUEST_SUCCESS");
						$redirect = "index.php";
					}
				} 
				catch (phpmailerException $e) 
				{
					$errors[] = $e->errorMessage();
				}				
			}
		}
	}
	if(count($errors) > 0)
	{
		$fields = stripslashes_array($_POST); // re-populate the form fields
		errorBlock($errors);
	}
	if(isset($success_message)) flash_message("success", $success_message);
	if(isset($redirect)) { header("location: ".$redirect); exit; }
	if(isset($fields)) return $fields;	
}
?>