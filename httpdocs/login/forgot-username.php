<?php
$ssl = true;
require_once('../admin/vertigo.php');
require_once('functions.php');

$pageTitle = "Username reminder";

//Prevent the user visiting this page if he/she is already logged in
if($user->loggedin == 1) { header("Location: ".SITE_URL."admin/users/routing.php"); exit(); }

$fields = forgotUsername();

include(SRV_ROOT."admin/includes/meta.php");
?>
</head>
<body class="login">
<div id="wrap" class="row">
<?php include(SRV_ROOT."admin/includes/header.php"); ?>
<div class="container">
	<div class="content">
		<?php
		//User feedback
		displayMessage();
		?>
		<form class="loginForm fourcol last centred" method="post" action="">
			<table>
				<caption>Username reminder</caption>
				<tr>
					<td colspan="2">Please enter your email address and first name below.<br/>Your username will be emailed to you</td>
				</tr>
				<tr>
					<th><label for="firstname">First name</label></th>
					<td><input id="firstname" type="text" name="firstname" value="<?=isset($fields['firstname'])?$fields['firstname']:''?>"/></td>
				</tr>
				<tr>
					<th><label for="email">Email</label></th>
					<td><input id="email" type="text" name="email" value="<?=isset($fields['email'])?$fields['email']:''?>"/></td>
				</tr>
				<tr>
					<td colspan="2">
						<span class="left"><a href="index.php">Back to login</a></span>
						<input name="submit" type="submit" class="button submit right" value="Submit"/>
					</td>
				</tr>
			</table>
		</form>
	</div>
</div>
</div>
<?php include(SRV_ROOT."admin/includes/footer.php"); ?>