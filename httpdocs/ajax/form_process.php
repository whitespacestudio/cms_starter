<?php
/**
 * Receives form submission
 * and sends it to the form processing function
 *
 * Outputs a JSON encoded array
 */

require_once(SRV_ROOT."includes/global-includes.php");
$result = formSubmit(false);
echo $result;
?>
