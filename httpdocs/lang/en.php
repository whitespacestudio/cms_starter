<?php
$lang = array_merge($lang,array(

"NAME"	=>	"Full name",
"EMAIL"	=>	"Email",
"PHONE"	=>	"Telephone number",
"COMMENT"	=>	"Comment",
"ATTACH_DOC"	=>	"Attach a document",
"UPLOAD_CV"	=>	"Upload your CV",
"MAX_FILESIZE"	=>	"Maximum file size 10Mb",
"ALLOWED_FILETYPES"	=>	"only PDF and MS Word documents accepted",
"SELECT_FILE"	=>	"Select file",
"UPLOAD_BTN"	=>	"Upload now",
"CAPTCHA_LABEL"	=>	"Enter security code",
"SEND"	=>	"Send",
"ERROR"	=>	"Error",
"FILE"	=>	"File",
"YOUR_NAME"	=>	"Your name",
"YOUR_EMAIL"	=>	"Your email",
"RCP_NAME"	=>	"Recipient's name",
"RCP_EMAIL"	=>	"Recipient's email",
"UPLOAD_SUCCESS"	=>	"File uploaded successfully. Please click \"Send\".",
"SEND_SUCCESS"	=>	"Thank you. Your message has been sent.",
"NAME_MISSING"	=>	"Your name is missing.",
"EMAIL_MISSING"	=>	"Your email address is missing.",
"EMAIL_INVALID"	=>	"Your email address is invalid.",
"PHONE_MISSING"	=>	"Your phone number is missing.",
"RCP_NAME_MISSING"	=>	"Recipient name is missing.",
"RCP_EMAIL_MISSING"	=>	"Recipient email address is missing.",
"RCP_EMAIL_INVALID"	=>	"Recipient email address is invalid",
"ERR_CAPTCHA"	=>	"The security code was incorrect",
"ERR_HEAD"	=>	"Oops, you missed a bit...",

"READMORE"	=>	"[read full post]",
"NO_POSTS_TITLE"	=>	"Nothing found",
"NO_POSTS_TEXT"	=>	"Sorry, no posts matched your criteria.",
"LATEST_POSTS"	=> "Latest posts",
"CATEGORIES"	=>	"Categories",
"ARCHIVES"	=>	"Archives",

"SEARCH_FIELD_PLC" => "Search",
"SEARCH_RESULTS_FOR" => "Search results for",
"EMPTY_SEARCH" => "Sorry, your search returned no results.",

"ACCOUNT_PASS_CHAR_LIMIT"				=> "Your password must be between %m1% and %m2% characters.",
"FORGOTPASS_INVALID_TOKEN"				=> "Sorry, this token is invalid.",
"FORGOTPASS_NEW_PASS_EMAIL"				=> "We have generated a new password and emailed it to you. Email is not very secure so please make sure you sign in and change it as soon as possible.",
"FORGOTPASS_REQUEST_CANNED"				=> "Your lost password request has been cancelled. Panic over.",
"FORGOTPASS_REQUEST_EXISTS"				=> "Hold your horses. There's already a lost password request on this account. If you didn't get the emai, you might want to give us a call.",
"FORGOTPASS_REQUEST_SUCCESS"			=> "We have received your request and have sent you a link to reset your password.  Please check your inbox and follow the instructions...",
"FORGOTUSERNAME_REQUEST_SUCCESS"		=> "We have emailed your username to the email address you supplied."

));
?>