<p>Hello #FIRSTNAME#</p>

<p>A lost password request has been submitted for your account on 
#DATE#.</p>

<p>To confirm this request, click here:</p>

<p>#CONFIRM-URL#

<p>To deny this request, click here:

<p>#DENY-URL#</p>