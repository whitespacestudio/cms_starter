<p>Hello #FIRSTNAME#</p>

<p>On #DATE# You (or someone else) asked for a reminder of your username for #SITE_NAME#.</p>

<p>Your username is:
	<br><br>
	<strong>#USERNAME#</strong>
	<br><br>
	To login, please visit #LOGIN_URL#. The username is case-sensitive, so you should enter it exactly as shown above, including any capital letters.
</p>

<p>If you have forgotten your password, you can reset it by visiting #PASSWORD_RESET_URL#</p>

<p>If you didn't make this request, rest assured that this message has only been delivered to this email address (#EMAIL#). If you have any concerns about the security of your account you should login and change your password as soon as possible.</p>

