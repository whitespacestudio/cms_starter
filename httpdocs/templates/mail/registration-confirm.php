<p>Hello #NAME#</p>

<p>Thank you for registering an account with #COMPANY-NAME#.</p>

<p>You can login and view your account history at any time by visiting #LOGIN-URL#</p>

<p>Your user name is: #USERNAME#</p>

<p>For security, we have not included your password.</p>