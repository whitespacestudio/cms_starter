<?php
include_once(SRV_ROOT."includes/global-includes.php");

/**
 * get_page_details() is defined in admin/pages/functions.php.
 * It queries the the database based on the current URL path and returns TRUE if a page is found, FALSE if not.
 * It also sets the following variables:
 * 
 * $view - the type of content to display. Used in the switch statement below
 * $this_slug - the url name for the current page. 
 * $parent_slug - the url name for the parent page (if any)
 * $tr_page - the page object.
 * $tr_parent - the parent page object
 */
$page_details = get_page_details();

if($page_details==false)
{
	$found = false;
	//Check for other page types here
	if(!$found)
	{
		//No page found. Set a 404 header and include the custom 404 page.
		header("HTTP/1.0 404 Not Found");
		include(SRV_ROOT."error_pages/404.php");
		exit;
	}
}
else
{
	//!Redirect alias and external page types, and the home page slug
	if($this_slug !='' && $tr_page->ishome)
	{
		header("HTTP/1.1 301 Moved Permanently");
		header("location: ".SITE_URL);
		exit;
	}
	//!Fix permalink for homepage
	if($tr_page->ishome)
	{
		$permalink = SITE_URL;
	}
	else
	{
		//!General page permalink
		$page_node = new sitemap_node('',$tr_page->id,'page');
		$permalink = SITE_URL.$page_node->path;
	}

	//Specific page type actions
	$page_type = $tr_page->get_page_type();
	switch($page_type)
	{
		case 'alias':
			$alias = new page($tr_page->type_val);
			if($alias->id !='')
			{
				$node = new sitemap_node('',$alias->id,'page');
				$node->path = $node->path == "/" ? "" : $node->path;
				header("HTTP/1.1 301 Moved Permanently");
				header("location: ".SITE_URL.$node->path);
				exit;
			}
		break;
		case 'external':
			header("HTTP/1.1 301 Moved Permanently");
			header("location: ".$tr_page->type_val);
			exit;
		break;
	}
	$found = true;
	
	//Pagination
	if(isset($_GET['page']) && preg_match("/^[0-9]{1,}$/", $_GET['page']))
	{
		$permalink .= "page".$_GET['page']."/"; 
	}
	if(isset($searchText))
	{
		$permalink = SITE_URL."search/".$searchText;
	}
}

//!Automatic 301 redirects
if(isset($permalink))
{
	$permalink_array = explode('/',$permalink);
    $permalink_end = end($permalink_array);
	if(file_ext($permalink_end) =='')
	{
		$url = parse_url(getCurrentURL());
		$currentURL = $url['scheme']."://".$url['host'].$url['path'];
		
		if(rtrim($currentURL, '/')."/" != $permalink)
		{
			header("HTTP/1.1 301 Moved Permanently");
			header("location: $permalink");
			exit;
		}
	}
}

//Set $tl_page the same as $tr_page to avoid errors when it's not defined
$tl_page = $tr_page;
//Include the <head> section (not including the closing </head> so you can add page specific head stuff)
include(SRV_ROOT."includes/meta.php");

//Include the necessary file based on the $view variable
switch($view)
{
	case 'home':
	
		include(SRV_ROOT."includes/content-home.php");

	break;	

	case 'page':
	
		include(SRV_ROOT."includes/content-page.php");

	break;

	case 'search':
	
		include(SRV_ROOT."includes/content-search.php");

	break;	

}

//Include the site footer and close <body> and <html>
include(SRV_ROOT."includes/footer.php");
?>
</body>
</html>