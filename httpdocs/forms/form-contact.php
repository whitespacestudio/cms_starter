<?php global $fields, $tr_page?>
<form name="contactForm" method="post" action="" id="contact_form">
	
	<input type="hidden" name="form" value="contact"/>
	<input type="hidden" name="type" value="contact"/>
	<input type="hidden" name="permalink" value="<?=getCurrentURL()?>"/>
	
	<div class="form_intro">
		<h2 class="alt">Ask us a question...</h2>
	</div>

	<fieldset>
		<input placeholder="Your name" type="text" name="name" value="<?= isset($fields['name'])?$fields['name']:''; ?>">
		<input placeholder="Your number" type="tel" name="phone" value="<?= isset($fields['phone'])?$fields['phone']:''; ?>">
		<input placeholder="Your email" type="text" name="email" value="<?= isset($fields['email'])?$fields['email']:''; ?>">
		<input placeholder="Kirk or Picard?" type="text" name="kirk_picard" value="<?= isset($fields['kirk_picard'])?$fields['kirk_picard']:''; ?>">
	</fieldset>
	<fieldset>
		<textarea placeholder="Your message" name="message" cols="24" rows="5" id="cont_message"><?= isset($fields['message'])?$fields['message']:''; ?></textarea>
		<input type="submit" name="submit" class="button" value="Send">
	</fieldset>

</form>