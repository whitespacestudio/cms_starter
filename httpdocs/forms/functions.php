<?php
function formSubmit($recipients)
{	
	global $globalSettings,$lang;
		
	if(isset($_POST['submit']))
	{	
		$errors = array(); // set the errors array to empty, by default
		$fields = array(); // stores the field values
		$rules = array(); // stores the validation rules

		//Dodge the bots
		if(isset($_POST['kirk_picard']) && $_POST['kirk_picard'] !='')
		{
			exit;
		}

		switch($_POST['form'])
		{			
			case "contact":
				$subject = "Online enquiry";
				$greeting = "You have received the following mesaage from the contact page.";
				$success = lang('SEND_SUCCESS');
				$rules[] = "required,name,Please enter your name.";
				$rules[] = "required,email,Please enter your email address.";
				$rules[] = "valid_email,email,Your email address looks a bit wrong.";
				$rules[] = "required,message,Please enter your message.";
			break;

		}		

		$errors = validateFields($_POST, $rules);

		if(isset($_POST['captcha']))
		{
			if($_SESSION['security_code'] == $_POST['captcha'] && !empty($_SESSION['security_code'])) 
			{
				unset($_SESSION['security_code']);
			}
			else
			{
				$errors[] = lang("ERR_CAPTCHA");
			}
		}
				
		switch($_POST['type'])
		{
			case "contact":
				//!CONTACT

				$origin				= isset($_POST['origin'])?$_POST['origin']:'';
				$salutation 		= isset($_POST['salutation'])?$_POST['salutation']:'';
				$name 				= isset($_POST['name'])?$_POST['name']:'';
				$firstname			= isset($_POST['firstname'])?$_POST['firstname']:'';
				$lastname			= isset($_POST['lastname'])?$_POST['lastname']:'';
				$name 				= !($firstname == '' && $lastname == '')?$firstname.' '.$lastname:$name;
				$name				= $salutation.' '.$name;
				$name				= trim($name);
				$address 			= isset($_POST['address'])?$_POST['address']:'';
				$town 				= isset($_POST['town'])?$_POST['town']:'';
				$postcode 			= isset($_POST['postcode'])?$_POST['postcode']:'';
				$email 				= isset($_POST['email'])?$_POST['email']:'';
				$phone				= isset($_POST['phone'])?$_POST['phone']:'';
				$mobile				= isset($_POST['mobile'])?$_POST['mobile']:'';
				$action				= isset($_POST['action'])?$_POST['action']:'';
				$best_time			= isset($_POST['best_time'])?$_POST['best_time']:'';
				$address			= isset($_POST['address'])?$_POST['address']:'';
				$message			= isset($_POST['message'])?$_POST['message']:'';
				$contactmethod		= isset($_POST['contactmethod'])?$_POST['contactmethod']:'';
				$file_attach 		= '';
				$permalink			= isset($_POST['permalink'])?$_POST['permalink']:SITE_URL;
												
				if (!empty($errors))
				{  
					$fields = stripslashes_array($_POST); // re-populate the form fields
					$message="<strong class=\"heading\">".lang("ERR_HEAD")."</strong>";
					errorBlock($errors,$message);
				}
				else 
				{
					$addresses = explode(",",$recipients);
	
					$html_content	 = "<p>$greeting</p>";
					if($_POST['form']=="notify" || $_POST['form']=="shippingquote")
					{
						$html_content .= "<strong>".$prod->title."</strong>";
					}
					$html_content	.= "<h2>Sender details</h2>";
					$html_content	.= "<p>";
					$html_content 	.= $origin != '' 		? 		"<strong>Page:</strong> ".stripslashes($origin)."<br/>" : "";
					$html_content 	.= $name != '' 			? 		"<strong>Name:</strong> ".stripslashes($name)."<br/>" : "";
					$html_content 	.= $address != '' 		? 		"<strong>Address:</strong><br/>".nl2br(stripslashes($address))."<br/>" : "";
					$html_content   .= $town != '' 			?		"<strong>Town:</strong> ".stripslashes($town)."<br/>" : "";
					$html_content   .= $postcode != '' 		? 		"<strong>Postcode:</strong> ".stripslashes($postcode)."<br/>" : "";
					$html_content   .= $phone != '' 		? 		"<strong>Phone:</strong> ".stripslashes($phone)."<br/>" : "";
					$html_content   .= $mobile != '' 		? 		"<strong>Mobile:</strong> ".stripslashes($mobile)."<br/>" : "";
					$html_content   .= $email != '' 		? 		"<strong>Email:</strong> ".stripslashes($email)."<br/>" : "";
					$html_content   .= $office != '' 		? 		"<strong>Office:</strong> ".stripslashes($office)."<br/>" : "";
					$html_content   .= $contactmethod != '' ? 		"<strong>Preferred contact method:</strong> ".stripslashes($contactmethod)."<br/>" : "";
					$html_content   .= $action != '' 		? 		"<strong>Action:</strong> ".stripslashes($action)."<br/>" : "";
					$html_content   .= $best_time != '' 	? 		"<strong>Best time to call:</strong> ".stripslashes($best_time)."<br/>" : "";
					
					$html_content   .= isset($nfy_details) && $nfy_details !='' ? "<strong>Product specifics:</strong>".stripslashes($nfy_details) : "";
						
					$html_content   .= "</p>";		

					$html_content   .= $message != '' 		? 		"<h2>Message:</h2>
												 					<p>".stripslashes(nl2br($message))."</p>" : "";
					
					$h2t = new \Html2Text\Html2Text($html_content);
					$plain_text = $h2t->get_text();
					
					$html = file_get_contents(SRV_ROOT.'templates/mail/email_header.php') .
							$html_content .
							file_get_contents(SRV_ROOT.'templates/mail/email_footer.php');
										
					$mail = new PHPMailer(true); 


					try 
					{
						if(isset($globalSettings['use_smtp']) && $globalSettings['use_smtp']=='true')
						{
							$mail->IsSMTP();
							$mail->Host = $globalSettings['smtp_hosts'];
							$mail->SMTPAuth = $globalSettings['smtp_auth'];
							$mail->Username = $globalSettings['smtp_user'];
							$mail->Password = $globalSettings['smtp_pass'];
							$mail->SMTPSecure = $globalSettings['smtp_security']; 
							$mail->Port       = $globalSettings['smtp_port'];
						}
						foreach($addresses as $recipient)
						{
							$mail->AddAddress($recipient);
						}
						$mail->SetFrom($globalSettings['admin_email'], $globalSettings['from_name']);
						$mail->AddReplyTo($globalSettings['admin_email'], $globalSettings['from_name']);
						$mail->Subject = $subject;
						$mail->AddEmbeddedImage(SRV_ROOT.'images/'.ELOGO, SESSNAME.'_logoimg', ELOGO);
						if($file_attach !='')
						{
							$mail->AddAttachment($file_attach);
						}

						$mail->MsgHTML($html);
						$mail->AltBody = $plain_text;
						if ($mail->Send())
						{
							flash_message("success", $success);
						}
						else
						{
							flash_message("error", "There was a problem sending your message. Please try again later");
						}
					} 
					catch (phpmailerException $e) 
					{
						flash_message("error", $e->errorMessage());
					}					

				}
			
			break;
			
			case 'login':
				//!LOGIN
				include_once(SRV_ROOT."login/functions.php");
				
				switch($_POST['form'])
				{
					case 'forgot-pass':
					
						$fields = forgotPassword();
					
					break;
				}
				
			break;
										
		}
		return $fields;
	}
}
?>