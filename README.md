# Traffic CMS starter template

A starter framework for building a website using the Traffic CMS.

### Installing

Copy all files and folders to your local development folder, then install the [Traffic CMS](https://bitbucket.org/nexusdp/traffic-cms) into the `httpdocs/admin/` folder.
The document root is the `httpdocs` folder. Any uncompressed/uncompiled files should be stored in the relevant folder in `src`. The idea is that this build is combined with the [CSS style guide](https://bitbucket.org/stephenelford/base-css-template) which should be placed outside of the root and compiled with any styles defined in `src/less`.

## Authors

* **Steve Elford** - *Initial build and ongoing work*
