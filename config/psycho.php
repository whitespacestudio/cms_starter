<?php
/*----------------------------------------------------------------------------------------------------------------------
CONFIGURATION
----------------------------------------------------------------------------------------------------------------------*/
$http_host = getenv('HTTP_HOST');
switch($http_host)
{
    case('dev.trevorblake.co.uk'):
		$dbtype 		= "mysqli";
		$db_host		= "localhost";
		$db_user		= "root";
		$db_pass		= "9Hvj2t3Q";
		$db_name		= "dev_trevorblake";
		$db_port		= "3306";
		$server_root	= '/Users/steve/Sites/trevorblake.co.uk/httpdocs/';
		$site_url		= $http_host;
		$display_errors	= 1;
		$ssl_enabled	= false;
		$environment	= 'Development';
	break;	
	default:
		$dbtype 		= "mysqli";
		$db_host		= "mysql.trafficservers.co.uk";
		$db_user		= "";
		$db_pass		= "";
		$db_name		= "";
		$db_port		= "3306";
		$server_root	= '/data/vhosts/';
		$site_url		= $http_host;
		$display_errors	= 0;
		$ssl_enabled	= false;
		$environment	= 'Production';
	break;
}

$db_table_prefix                = "";									   // Prefix string for table names (Not yet supported)
$langauge                       = "en";									   // CMS language (Not yet supported)
$uploads_directory              = 'uploads/';						   	   // Location of uploads folder relative to the website root
$site_name                      = 'Trevor Blake';						   // The default name of the site
$session_name                   = 'trevorblake';						   // The session name - no spaces or special characters
$new_user_email_activation      = false;								   // Require email activation for new users
$new_account_notify_user        = false;								   // Email login details to new users
$email_logo                     = 'emaillogo.jpg';						   // Filename of the logo to attach to emails
$site_language                  = 'en';									   // Default site language
$resend_activation_threshold    = 0;									   // 
$remember_me_length             = "4wk";								   // How long before the 'remember me' cookie expires
$error_reporting				= E_ALL;								   // Specify the type of error messages to report in the log

/*----------------------------------------------------------------------------------------------------------------------
DO NOT EDIT BELOW THIS LINE
----------------------------------------------------------------------------------------------------------------------*/
//Add trailing slashes where needed if not present
$server_root = rtrim($server_root, '/')."/";
$site_url = rtrim($site_url, '/')."/";
$uploads_directory = rtrim($uploads_directory, '/')."/";

include_once($server_root."admin/vertigo.php");
?>